package com.reobotnet.cobranca.service;

import java.io.Serializable;

import javax.inject.Inject;

import com.reobotnet.cobranca.model.Cobranca;
import com.reobotnet.cobranca.model.Status;
import com.reobotnet.cobranca.repository.Cobrancas;
import com.reobotnet.cobranca.util.jpa.Transactional;



public class NovaCobrancaService implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private Cobrancas cobrancas;
	
	@Transactional
	public Cobranca salvar(Cobranca cobranca) {
		cobranca.setStatus(Status.PENDENTE);
		cobranca = this.cobrancas.guardar(cobranca);
		
		return cobranca;
	}

}